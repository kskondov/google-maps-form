var validator = (function() {
    var regexValidate = function(toValidate, regex) {
        regex = new RegExp(regex);
        if (!regex.test(toValidate.trim())) {
            return false;
        } else {
            return true;
        }
    };

    return {
        regexValidate: regexValidate
    };
})();
var controller = (function() {

    var nameRegex = /^[A-Za-z\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$/,
        phoneNumberRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/,
        emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        websiteRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        addressRegex = / /;
    var user;

    function control() {
        $("#submit").click(function() {
            if (!validator.regexValidate($("#name").val(), nameRegex) || !validator.regexValidate($("#address").val()) || !validator.regexValidate($("#email").val(), emailRegex) || !validator.regexValidate($("#phone").val(), phoneNumberRegex) || !validator.regexValidate($("#website").val(), websiteRegex)) {
                failedValidationLabelChange($("#name"), "name", nameRegex);
                failedValidationLabelChange($("#address"), "address", addressRegex);
                failedValidationLabelChange($("#email"), "E-Mail", emailRegex);
                failedValidationLabelChange($("#phone"), "phone number", phoneNumberRegex)
                failedValidationLabelChange($("#website"), "website", websiteRegex);
                return;
            } else {
                passedValidationLabelChange($("#name"), "name");
                passedValidationLabelChange($("#address"), "address");
                passedValidationLabelChange($("#email"), "E-Mail");
                passedValidationLabelChange($("#phone"), "phone number");
                passedValidationLabelChange($("#website"), "website");

                user = new userModel.User($('#name').val(), $('#address').val(), $('#email').val(), $('#phone').val(), $('#website').val());
                view.initMap();
                view.geocodeAddress(user.address);
                localStorage.setItem('newuser', JSON.stringify(user));
            }
        });

        $("#findAddress").click(function() {
            view.initMap();
        });

        function failedValidationLabelChange(element, message, regex) {
            var regexCheck = new RegExp(regex),
                label = $("label[for='" + $(element).attr('id') + "']");

            if (!validator.regexValidate($(element).val(), regexCheck)) {
                $(element).val("");
                $(element).attr("placeholder", "required");
                label.text("Please enter valid " + message).css('color', 'red');
            } else {

                passedValidationLabelChange(element, message);
            }
        }

        function passedValidationLabelChange(element, message) {
            var label = $("label[for='" + $(element).attr('id') + "']");
            label.text(message.charAt(0).toUpperCase() + message.slice(1) + ":").css('color', 'black');
        }
    }
    return {
        control: control
    };
}());
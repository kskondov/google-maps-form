var userModel = (function() {

    function User(name, address, email, phone, website) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.website = website;
    }

    return {
        User: User
    };
}());
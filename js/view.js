var view = (function view() {
    var markersArray = [];
    var map,
        geoCoder = new google.maps.Geocoder(),
        address,
        initialLatlng = { lat: -13.000, lng: 13.000 },
        latitude,
        longitude;

    function initMap() {
        $("#map").show();
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: initialLatlng
        });
        map.addListener('click', function(e) {
            latitude = e.latLng.lat();
            longitude = e.latLng.lng();
            placeMarkerAndPanTo(e.latLng, map);
            geocodeLatLng();
        });
    }

    function geocodeAddress(address) {
        if (address.length === 0) {
            alert("message");
            return;
        } else {
            geoCoder.geocode({
                'address': address
            }, function(results, status) {
                if (status === 'OK') {
                    map.setCenter(results[0].geometry.location);
                    map.setZoom(13);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    $("#address").val(results[0].formatted_address);
                    markersArray.push(marker);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
    }

    function geocodeLatLng() {
        var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
        geoCoder.geocode({ 'location': latlng }, function(results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    $("#address").val(results[1].formatted_address);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('GeoCoder failed due to: ' + status);
            }
        });
    }

    function placeMarkerAndPanTo(latLng, map) {
        deleteOverlays();
        var marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        map.panTo(latLng);
        markersArray.push(marker);
    }

    function deleteOverlays() {
        if (markersArray) {
            var i;
            for (i = 0; i < markersArray.length; i += 1) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
    }

    return {
        initMap: initMap,
        geocodeAddress: geocodeAddress
    };
}());